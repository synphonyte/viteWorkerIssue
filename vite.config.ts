import { defineConfig } from 'vite';
import { createVuePlugin } from 'vite-plugin-vue2';
import path from 'path';


const srcDir = path.resolve( __dirname, 'src' );

// https://vitejs.dev/config/
export default defineConfig( {
  resolve: {
    alias: {
      '@': srcDir,
    },
  },

  plugins: [ createVuePlugin( {} ) ],

  build: {
    target: 'es2019',
  },
} );
