export function sum(base: number, add: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(base + add), 300);
  })
}